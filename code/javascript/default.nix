
let

  allScripts = [
    "javascript-binarytrees-01"
    "javascript-binarytrees-03"
    "javascript-fannkuchredux-01"
    "javascript-fannkuchredux-04"
    "javascript-fasta-01"
    "javascript-fasta-02"
    "javascript-fasta-03"
    "javascript-fasta-04"
    "javascript-knucleotide-01"
    "javascript-knucleotide-02"
    "javascript-mandelbrot-01"
    "javascript-nbody-01"
    "javascript-nbody-02"
    "javascript-nbody-04"
    "javascript-nbody-05"
    "javascript-pidigits-03"
    "javascript-pidigits-04"
    "javascript-regexredux-01"
    "javascript-regexredux-02"
    "javascript-revcomp-02"
    "javascript-revcomp-07"
    "javascript-spectralnorm-01"
    "javascript-spectralnorm-02"
    "javascript-spectralnorm-03"
    "javascript-spectralnorm-05"
  ];

  pkgs = import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/19.03.tar.gz") {};

  nodejs = pkgs.nodejs-11_x;

  addScript = aux: script: aux + ''
    printf "#!${nodejs.outPath}/bin/node\n\n" > $out/bin/${script}
    chmod +x $out/bin/${script}
    cat src/${script}.js >> $out/bin/${script}
  '';

  foldl = pkgs.lib.lists.foldl;

in

  pkgs.stdenv.mkDerivation {
    name = "my-own-benchmarks-game-javascript";
    src = ./.;

    installPhase = ''
      mkdir -p $out/bin
    '' + foldl addScript "" allScripts;

  }

