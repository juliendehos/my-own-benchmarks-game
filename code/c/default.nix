
let

  pkgs = import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/19.03.tar.gz") {};

  stdenv = pkgs.overrideCC pkgs.stdenv pkgs.gcc8;

  # c-knucleotide-01
  klib = stdenv.mkDerivation {
    name = "klib";
    src = pkgs.fetchurl {
      url = "https://github.com/attractivechaos/klib/archive/master.tar.gz";
      sha256 = "0fafixi21drc2k7g5lg57zxksbaf33rwhvpjbgk0c2jpb4bw6sl7";
    };
    installPhase = ''
      mkdir -p $out/include
      cp *.h $out/include/
    '';
  };

  opts = "-pipe -Wall -O3 -fomit-frame-pointer -march=native";

in

  stdenv.mkDerivation {
    name = "my-own-benchmarks-game-c";
    src = ./.;

    buildInputs = [
      klib
      pkgs.apr
      pkgs.gmp
      pkgs.pkgconfig
      pkgs.pcre
      pkgs.pcre2
    ];

    buildPhase = ''

      gcc -o c-binarytrees-01 ${opts} src/c-binarytrees-01.c -lm
      gcc -o c-binarytrees-02 ${opts} -fopenmp src/c-binarytrees-02.c `pkg-config --libs apr-1` 
      gcc -o c-binarytrees-03 ${opts} -fopenmp -D_FILE_OFFSET_BITS=64 src/c-binarytrees-03.c `pkg-config --libs apr-1` -lgomp -lm
      gcc -o c-binarytrees-05 ${opts} -pthread src/c-binarytrees-05.c

      gcc -o c-fannkuchredux-01 ${opts} src/c-fannkuchredux-01.c
      gcc -o c-fannkuchredux-02 ${opts} -pthread src/c-fannkuchredux-02.c
      gcc -o c-fannkuchredux-03 ${opts} src/c-fannkuchredux-03.c
      gcc -o c-fannkuchredux-04 ${opts} -msse3 -msse4.1 src/c-fannkuchredux-04.c
      gcc -o c-fannkuchredux-05 ${opts} -fopenmp src/c-fannkuchredux-05.c

      gcc -o c-fasta-01 ${opts} src/c-fasta-01.c
      gcc -o c-fasta-02 ${opts} -fopenmp src/c-fasta-02.c
      gcc -o c-fasta-04 ${opts} src/c-fasta-04.c
      gcc -o c-fasta-05 ${opts} src/c-fasta-05.c
      gcc -o c-fasta-06 ${opts} -fopenmp src/c-fasta-06.c
      gcc -o c-fasta-07 ${opts} -fopenmp src/c-fasta-07.c

      gcc -o c-knucleotide-01 ${opts} -fopenmp src/c-knucleotide-01.c

      gcc -o c-mandelbrot-01 ${opts} -pthread src/c-mandelbrot-01.c
      gcc -o c-mandelbrot-02 ${opts} src/c-mandelbrot-02.c
      gcc -o c-mandelbrot-03 ${opts} -pthread src/c-mandelbrot-03.c
      gcc -o c-mandelbrot-04 ${opts} -fopenmp src/c-mandelbrot-04.c
      gcc -o c-mandelbrot-06 ${opts} -mno-fma -fno-finite-math-only -fopenmp src/c-mandelbrot-06.c
      gcc -o c-mandelbrot-07 ${opts} -fopenmp src/c-mandelbrot-07.c
      gcc -o c-mandelbrot-08 ${opts} -fopenmp src/c-mandelbrot-08.c
      gcc -o c-mandelbrot-09 ${opts} -fopenmp src/c-mandelbrot-09.c

      gcc -o c-nbody-01 ${opts} -mfpmath=sse -msse3 src/c-nbody-01.c -lm
      gcc -o c-nbody-02 ${opts} -mfpmath=sse -msse3 src/c-nbody-02.c -lm
      gcc -o c-nbody-03 ${opts} -mfpmath=sse -msse3 src/c-nbody-03.c -lm
      gcc -o c-nbody-04 ${opts} -mfpmath=sse -msse3 src/c-nbody-04.c -lm
      gcc -o c-nbody-05 ${opts} -mfpmath=sse -msse3 src/c-nbody-05.c -lm
      gcc -o c-nbody-06 ${opts} -mfpmath=sse -msse3 src/c-nbody-06.c -lm
      gcc -o c-nbody-07 ${opts} -mfpmath=sse -msse3 src/c-nbody-07.c -lm
      gcc -o c-nbody-08 ${opts} -funroll-loops src/c-nbody-08.c -lm

      gcc -o c-pidigits-01 ${opts} src/c-pidigits-01.c -lgmp
      gcc -o c-pidigits-02 ${opts} src/c-pidigits-02.c -lgmp

      gcc -o c-regexredux-02 ${opts} src/c-regexredux-02.c `pkg-config --libs libpcre`
      gcc -o c-regexredux-03 ${opts} -fopenmp src/c-regexredux-03.c `pkg-config --libs libpcre`
      gcc -o c-regexredux-04 ${opts} -fopenmp src/c-regexredux-04.c `pkg-config --libs libpcre`
      gcc -o c-regexredux-05 ${opts} -fopenmp src/c-regexredux-05.c `pkg-config --libs libpcre2-posix libpcre2-8`

      gcc -o c-revcomp-01 ${opts} -pthread src/c-revcomp-01.c
      gcc -o c-revcomp-02 ${opts} -pthread src/c-revcomp-02.c
      gcc -o c-revcomp-03 ${opts} -pthread src/c-revcomp-03.c
      gcc -o c-revcomp-04 ${opts} src/c-revcomp-04.c
      gcc -o c-revcomp-05 ${opts} src/c-revcomp-05.c
      gcc -o c-revcomp-06 ${opts} -fopenmp src/c-revcomp-06.c

      gcc -o c-spectralnorm-01 ${opts} -mfpmath=sse -msse3 -fopenmp src/c-spectralnorm-01.c -lm
      gcc -o c-spectralnorm-03 ${opts} -mfpmath=sse -msse3 -fopenmp src/c-spectralnorm-03.c -lm
      gcc -o c-spectralnorm-04 ${opts} -mfpmath=sse -msse3 -fopenmp src/c-spectralnorm-04.c -lm
      gcc -o c-spectralnorm-05 ${opts} -mfpmath=sse -msse3 -fopenmp src/c-spectralnorm-05.c -lm

    '';

    installPhase = ''
      mkdir -p $out/bin
      cp c-* $out/bin
    '';
  }

