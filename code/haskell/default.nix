
let

  pkgs = import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/19.03.tar.gz") {};

  ghc = pkgs.haskell.packages.ghc864.ghcWithPackages (ps: [
    ps.async
    ps.hashtables
    ps.parallel
    ps.regex-pcre
  ]);

  opts = "--make -fllvm -O2 -threaded -rtsopts";

in

  pkgs.stdenv.mkDerivation {
    name = "my-own-benchmarks-game-haskell";
    src = ./.;

    buildInputs = [
      ghc
      pkgs.llvm_6
    ];

    buildPhase = ''

      ghc -o haskell-binarytrees-01 ${opts} -with-rtsopts="-N4 -K128M -H" src/haskell-binarytrees-01.hs
      ghc -o haskell-binarytrees-02 ${opts} -with-rtsopts="-N4 -K128M -H" src/haskell-binarytrees-02.hs
      ghc -o haskell-binarytrees-03 ${opts} -with-rtsopts="-N4 -K128M -H" src/haskell-binarytrees-03.hs
      #ghc -o haskell-binarytrees-04 ${opts} -with-rtsopts="-N4 -K128M -H" src/haskell-binarytrees-04.hs
      ghc -o haskell-binarytrees-05 ${opts} -with-rtsopts="-N4 -K128M -H" src/haskell-binarytrees-05.hs
      ghc -o haskell-binarytrees-06 ${opts} -with-rtsopts="-N4 -K128M -H" src/haskell-binarytrees-06.hs

      ghc -o haskell-fannkuchredux-01 ${opts} -with-rtsopts="-N4" src/haskell-fannkuchredux-01.hs
      ghc -o haskell-fannkuchredux-02 ${opts} -with-rtsopts="-N4" src/haskell-fannkuchredux-02.hs
      #ghc -o haskell-fannkuchredux-03 ${opts} -with-rtsopts="-N4" src/haskell-fannkuchredux-03.hs
      ghc -o haskell-fannkuchredux-04 ${opts} -with-rtsopts="-N4" src/haskell-fannkuchredux-04.hs
      ghc -o haskell-fannkuchredux-05 ${opts} -with-rtsopts="-N4" src/haskell-fannkuchredux-05.hs
      ghc -o haskell-fannkuchredux-06 ${opts} -with-rtsopts="-N4" src/haskell-fannkuchredux-06.hs

      ghc -o haskell-fasta-01 ${opts} -with-rtsopts="-N4" src/haskell-fasta-01.hs
      ghc -o haskell-fasta-02 ${opts} -with-rtsopts="-N4" src/haskell-fasta-02.hs
      ghc -o haskell-fasta-03 ${opts} -with-rtsopts="-N4" src/haskell-fasta-03.hs

      ghc -o haskell-knucleotide-01 ${opts} -funbox-strict-fields -with-rtsopts="-N4 -K2048M" src/haskell-knucleotide-01.hs
      ghc -o haskell-knucleotide-02 ${opts} -funbox-strict-fields -with-rtsopts="-N4 -K2048M" src/haskell-knucleotide-02.hs

      ghc -o haskell-nbody-01 ${opts} -with-rtsopts="-N4" src/haskell-nbody-01.hs
      ghc -o haskell-nbody-02 ${opts} -with-rtsopts="-N4" src/haskell-nbody-02.hs

      ghc -o haskell-pidigits-01 ${opts} -with-rtsopts="-N4" src/haskell-pidigits-01.hs
      ghc -o haskell-pidigits-02 ${opts} -with-rtsopts="-N4" src/haskell-pidigits-02.hs
      ghc -o haskell-pidigits-03 ${opts} -with-rtsopts="-N4" src/haskell-pidigits-03.hs
      ghc -o haskell-pidigits-04 ${opts} -with-rtsopts="-N4" src/haskell-pidigits-04.hs

      ghc -o haskell-revcomp-02 ${opts} -funfolding-use-threshold=32 -with-rtsopts="-N4" src/haskell-revcomp-02.hs
      ghc -o haskell-revcomp-03 ${opts} -funfolding-use-threshold=32 -with-rtsopts="-N4" src/haskell-revcomp-03.hs

      ghc -o haskell-spectralnorm-02 ${opts} -with-rtsopts="-N4" src/haskell-spectralnorm-02.hs
      ghc -o haskell-spectralnorm-04 ${opts} -with-rtsopts="-N4" src/haskell-spectralnorm-04.hs

    '';

    installPhase = ''
      mkdir -p $out/bin
      cp haskell-* $out/bin
    '';

    shellHook = "eval $(egrep ^export ${ghc}/bin/ghc)";
  }

