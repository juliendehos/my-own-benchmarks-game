
let

  pkgs = import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/19.03.tar.gz") {};

  python = pkgs.python37.withPackages (ps: with ps; [ gmpy2 ]);

in

  pkgs.stdenv.mkDerivation {
    name = "my-own-benchmarks-game-python";
    src = ./.;

    installPhase = ''
      mkdir -p $out/bin
      codes=`find src -name "*.py" | cut -d "/" -f 2 | cut -d "." -f 1`
      for code in $codes ; do
        printf "#!${python.interpreter}\n\n" > $out/bin/$code
        chmod +x $out/bin/$code
        cat src/$code.py >> $out/bin/$code
      done
    '';

  }

