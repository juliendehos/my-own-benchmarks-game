
let

  pkgs = import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/19.03.tar.gz") {};

in

  pkgs.stdenv.mkDerivation {
    name = "my-own-benchmarks-game-java";
    src = ./.;

    nativeBuildInputs = [
      pkgs.makeWrapper
      pkgs.jdk
    ];

    buildPhase = ''
      codes=`find src -name "*.java" | cut -d "/" -f 2 | cut -d "." -f 1`
      for code in $codes ; do
        mkdir -p mobg/$code
        cp src/$code.java mobg/$code/
        javac mobg/$code/$code.java
      done
    '';

    installPhase = ''
      mkdir -p $out/{bin,share}
      cp -R mobg $out/share/
      codes=`find src -name "*.java" | cut -d "/" -f 2 | cut -d "." -f 1`
      for code in $codes ; do
        makeWrapper ${pkgs.jre}/bin/java $out/bin/$code --add-flags "-cp $out/share/mobg/$code Main"
      done
    '';

  }

