
let

  pkgs = import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/19.03.tar.gz") {};

  stdenv = pkgs.overrideCC pkgs.stdenv pkgs.gcc8;

  opts = "-pipe -O3 -fomit-frame-pointer -march=native";

in

  stdenv.mkDerivation {
    name = "my-own-benchmarks-game-cpp";
    src = ./.;

    buildInputs = [
      pkgs.apr
      pkgs.boost
      pkgs.gmpxx
      pkgs.pcre
      pkgs.pkgconfig
      pkgs.re2
    ];

    buildPhase = ''

      g++ -o cpp-binarytrees-01 ${opts} -pthread src/cpp-binarytrees-01.cpp
      g++ -o cpp-binarytrees-02 ${opts} src/cpp-binarytrees-02.cpp
      g++ -o cpp-binarytrees-03 ${opts} -pthread src/cpp-binarytrees-03.cpp
      g++ -o cpp-binarytrees-06 ${opts} -fopenmp src/cpp-binarytrees-06.cpp
      g++ -o cpp-binarytrees-08 ${opts} -fopenmp src/cpp-binarytrees-08.cpp
      g++ -o cpp-binarytrees-09 ${opts} -fopenmp src/cpp-binarytrees-09.cpp `pkg-config --libs apr-1` 

      g++ -o cpp-fannkuchredux-01 ${opts} -pthread src/cpp-fannkuchredux-01.cpp
      g++ -o cpp-fannkuchredux-02 ${opts} -pthread src/cpp-fannkuchredux-02.cpp -lboost_thread -lboost_system
      g++ -o cpp-fannkuchredux-03 ${opts} src/cpp-fannkuchredux-03.cpp
      g++ -o cpp-fannkuchredux-04 ${opts} -pthread src/cpp-fannkuchredux-04.cpp -lboost_thread -lboost_system
      g++ -o cpp-fannkuchredux-05 ${opts} -fopenmp src/cpp-fannkuchredux-05.cpp
      g++ -o cpp-fannkuchredux-07 ${opts} -msse3 -msse4.1 -pthread src/cpp-fannkuchredux-07.cpp

      g++ -o cpp-fasta-01 ${opts} -mfpmath=sse -msse3 src/cpp-fasta-01.cpp
      g++ -o cpp-fasta-02 ${opts} -mfpmath=sse -msse3 src/cpp-fasta-02.cpp
      g++ -o cpp-fasta-03 ${opts} -mfpmath=sse -msse3 src/cpp-fasta-03.cpp
      g++ -o cpp-fasta-04 ${opts} -mfpmath=sse -msse3 src/cpp-fasta-04.cpp
      g++ -o cpp-fasta-05 ${opts} -mfpmath=sse -msse3 -pthread src/cpp-fasta-05.cpp
      g++ -o cpp-fasta-06 ${opts} -mfpmath=sse -msse3 -pthread src/cpp-fasta-06.cpp
      g++ -o cpp-fasta-07 ${opts} -mfpmath=sse -msse3 -pthread -std=c++17 -fconcepts src/cpp-fasta-07.cpp

      g++ -o cpp-knucleotide-01 ${opts} -pthread src/cpp-knucleotide-01.cpp
      g++ -o cpp-knucleotide-02 ${opts} -pthread -std=c++17 -fconcepts src/cpp-knucleotide-02.cpp
      g++ -o cpp-knucleotide-03 ${opts} -pthread -Wl,--no-as-needed src/cpp-knucleotide-03.cpp

      g++ -o cpp-mandelbrot-01 ${opts} -msse4.1 -msse3 -fopenmp src/cpp-mandelbrot-01.cpp
      g++ -o cpp-mandelbrot-02 ${opts} -mfpmath=sse -msse3 src/cpp-mandelbrot-02.cpp
      g++ -o cpp-mandelbrot-03 ${opts} -mfpmath=sse -msse3 src/cpp-mandelbrot-03.cpp
      g++ -o cpp-mandelbrot-04 ${opts} -mfpmath=sse -msse3 -fopenmp src/cpp-mandelbrot-04.cpp
      g++ -o cpp-mandelbrot-05 ${opts} -mfpmath=sse -msse3 -fopenmp src/cpp-mandelbrot-05.cpp
      g++ -o cpp-mandelbrot-06 ${opts} -mfpmath=sse -msse3 -fopenmp -mno-fma src/cpp-mandelbrot-06.cpp
      g++ -o cpp-mandelbrot-07 ${opts} -mfpmath=sse -msse3 -fopenmp src/cpp-mandelbrot-07.cpp
      g++ -o cpp-mandelbrot-08 ${opts} -mfpmath=sse -msse3 -fopenmp src/cpp-mandelbrot-08.cpp
      g++ -o cpp-mandelbrot-09 ${opts} -mfpmath=sse -msse3 -fopenmp src/cpp-mandelbrot-09.cpp

      g++ -o cpp-nbody-01 ${opts} -mfpmath=sse -msse3 src/cpp-nbody-01.cpp
      g++ -o cpp-nbody-03 ${opts} -mfpmath=sse -msse3 src/cpp-nbody-03.cpp
      g++ -o cpp-nbody-04 ${opts} -mfpmath=sse -msse3 src/cpp-nbody-04.cpp
      g++ -o cpp-nbody-05 ${opts} -mfpmath=sse -msse3 src/cpp-nbody-05.cpp
      g++ -o cpp-nbody-06 ${opts} -mfpmath=sse -msse3 src/cpp-nbody-06.cpp
      g++ -o cpp-nbody-07 ${opts} -mfpmath=sse -msse3 src/cpp-nbody-07.cpp
      g++ -o cpp-nbody-08 ${opts} -mfpmath=sse -msse3 src/cpp-nbody-08.cpp
      g++ -o cpp-nbody-09 ${opts} -mfpmath=sse -msse3 src/cpp-nbody-09.cpp
      g++ -o cpp-nbody-14 ${opts} -mfpmath=sse -msse3 src/cpp-nbody-14.cpp
      g++ -o cpp-nbody-18 ${opts} -mfpmath=sse -msse3 src/cpp-nbody-18.cpp
      g++ -o cpp-nbody-19 ${opts} -mfpmath=sse -msse3 src/cpp-nbody-19.cpp

      g++ -o cpp-pidigits-04 ${opts} src/cpp-pidigits-04.cpp -lgmp -lgmpxx 

      g++ -o cpp-regexredux-01 ${opts} -pthread src/cpp-regexredux-01.cpp -lre2
      g++ -o cpp-regexredux-02 ${opts} -fopenmp src/cpp-regexredux-02.cpp -lre2
      g++ -o cpp-regexredux-03 ${opts} src/cpp-regexredux-03.cpp -lboost_regex
      g++ -o cpp-regexredux-04 ${opts} -std=c++17 -fopenmp -flto src/cpp-regexredux-04.cpp `pkg-config --libs libpcre`
      g++ -o cpp-regexredux-05 ${opts} -pthread src/cpp-regexredux-05.cpp -lre2

      g++ -o cpp-revcomp-01 ${opts} src/cpp-revcomp-01.cpp
      g++ -o cpp-revcomp-02 ${opts} -fopenmp src/cpp-revcomp-02.cpp
      g++ -o cpp-revcomp-03 ${opts} src/cpp-revcomp-03.cpp
      g++ -o cpp-revcomp-04 ${opts} -mtune=native -mfpmath=sse -msse2 -pthread src/cpp-revcomp-04.cpp
      g++ -o cpp-revcomp-05 ${opts} -mtune=native -mfpmath=sse -msse2 -pthread src/cpp-revcomp-05.cpp
      g++ -o cpp-revcomp-06 ${opts} -mtune=native -mfpmath=sse -msse2 -pthread src/cpp-revcomp-06.cpp

      g++ -o cpp-spectralnorm-01 ${opts} -mfpmath=sse -msse3 src/cpp-spectralnorm-01.cpp
      g++ -o cpp-spectralnorm-05 ${opts} -mfpmath=sse -msse3 -fopenmp src/cpp-spectralnorm-05.cpp
      g++ -o cpp-spectralnorm-06 -pipe -Os -fomit-frame-pointer -march=native -mfpmath=sse -msse3 -fopenmp src/cpp-spectralnorm-06.cpp
      g++ -o cpp-spectralnorm-08 -pipe -Os -fomit-frame-pointer -march=native -mfpmath=sse -msse3 -fopenmp src/cpp-spectralnorm-08.cpp

    '';

    installPhase = ''
      mkdir -p $out/bin
      cp cpp-* $out/bin
    '';
  }

