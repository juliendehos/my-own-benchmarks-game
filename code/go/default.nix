
let

  pkgs = import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/19.03.tar.gz") {};

  app = pkgs.buildGoPackage {
    name = "app";
    src = ./src ;
    goPackagePath = "my-own-benchmarks-game-go";
    goDeps = ./deps.nix;

    buildInputs = [
      pkgs.gmp
      pkgs.pcre
    ];

    subPackages = [

      "go-binarytrees-01"
      "go-binarytrees-02"
      "go-binarytrees-03"
      "go-binarytrees-04"
      "go-binarytrees-05"
      "go-binarytrees-06"
      "go-binarytrees-07"
      "go-binarytrees-09"

      "go-fannkuchredux-01"
      "go-fannkuchredux-02"

      "go-fasta-01"
      "go-fasta-02"
      "go-fasta-03"

      "go-nbody-01"
      "go-nbody-02"
      "go-nbody-03"

      "go-knucleotide-01"
      "go-knucleotide-02"
      "go-knucleotide-03"
      "go-knucleotide-04"
      "go-knucleotide-06"
      "go-knucleotide-07"

      "go-mandelbrot-01"
      "go-mandelbrot-02"
      "go-mandelbrot-03"
      "go-mandelbrot-04"
      "go-mandelbrot-06"

      "go-pidigits-01"
      "go-pidigits-02"
      "go-pidigits-03"

      "go-regexredux-01"
      "go-regexredux-03"
      "go-regexredux-04"

      "go-revcomp-01"
      "go-revcomp-02"
      "go-revcomp-03"
      "go-revcomp-05"
      "go-revcomp-06"

      "go-spectralnorm-01"
      "go-spectralnorm-02"
      "go-spectralnorm-04"

    ];

  };

in

  pkgs.stdenv.mkDerivation {
    name = "my-own-benchmarks-game-go";
    src = ./. ;
    installPhase = ''
      mkdir -p $out/bin
      cp ${app}/bin/* $out/bin/
    '';
   }

