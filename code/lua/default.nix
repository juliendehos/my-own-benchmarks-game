
let

  pkgs = import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/19.03.tar.gz") {};

in

  # pkgs.stdenv.buildLuaPackage {
  #   name = "my-own-benchmarks-game-lua";
  #   src = ./src;
  #   buildInputs = [ pkgs.gmp ];
  # }


  pkgs.stdenv.mkDerivation {
    name = "my-own-benchmarks-game-lua";
    src = ./.;

    installPhase = ''
      mkdir -p $out/bin
      codes=`find src -name "*.lua" | cut -d "/" -f 2 | cut -d "." -f 1`
      for code in $codes ; do
        printf "#!${pkgs.lua.interpreter}\n\n" > $out/bin/$code
        chmod +x $out/bin/$code
        cat src/$code.lua >> $out/bin/$code
      done
    '';

  }

