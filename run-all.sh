#!/bin/sh

NRUNS=2
LANGS=`find code/ -maxdepth 1 -mindepth 1 | cut -d "/" -f 2`

echo $LANGS

rm -rf build output

for lang in $LANGS ; do
    rm -rf build/code/$lang
    ./scripts/bench.sh $NRUNS output/output-$lang `find code/$lang -name "$lang-*-*.*"`
done

./scripts/merge.sh output/all output/output-*
./scripts/plot.py output/all
./scripts/render.hs output/all

