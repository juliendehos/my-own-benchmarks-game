
let

  allScripts = [
    "javascript-binarytrees-11"
    "javascript-fannkuchredux-11"
    "javascript-fannkuchredux-12"
    "javascript-fannkuchredux-13"
    "javascript-fasta-11"
    "javascript-fasta-12"
    "javascript-knucleotide-11"
    "javascript-knucleotide-12"
    "javascript-mandelbrot-12"
    "javascript-nbody-11"
    "javascript-nbody-12"
    "javascript-revcomp-11"
    "javascript-revcomp-12"
    "javascript-revcomp-13"
    "javascript-revcomp-14"
    "javascript-revcomp-16"
    "javascript-spectralnorm-11"
    "javascript-spectralnorm-12"
    "javascript-spectralnorm-13"
    "javascript-spectralnorm-15"
  ];

  pkgs = import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/19.03.tar.gz") {};

  nodejs = pkgs.nodejs-11_x;

  addScript = aux: script: aux + ''
    printf "#!${nodejs.outPath}/bin/node\n\n" > $out/bin/${script}
    chmod +x $out/bin/${script}
    cat src/${script}.js >> $out/bin/${script}
  '';

  foldl = pkgs.lib.lists.foldl;

in

  pkgs.stdenv.mkDerivation {
    name = "my-own-benchmarks-game-javascript";
    src = ./.;

    installPhase = ''
      mkdir -p $out/bin
    '' + foldl addScript "" allScripts;

  }

