
let

  pkgs = import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/19.03.tar.gz") {};

  app = pkgs.buildGoPackage {
    name = "app";
    src = ./src ;
    goPackagePath = "my-own-benchmarks-game-go";
    goDeps = ./deps.nix;

    buildInputs = [
      pkgs.gmp
      pkgs.pcre
    ];

    subPackages = [

      "go-regexredux-02"

    ];

  };

in

  pkgs.stdenv.mkDerivation {
    name = "my-own-benchmarks-game-go";
    src = ./. ;
    installPhase = ''
      mkdir -p $out/bin
      cp ${app}/bin/* $out/bin/
    '';
   }

