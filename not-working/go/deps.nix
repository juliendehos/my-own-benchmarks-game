[
  {
    goPackagePath = "github.com/ncw/gmp";
    fetch = {
      type = "git";
      url = "https://github.com/ncw/gmp";
      rev = "v1.0.3";
      sha256 = "01azpfhi4d7hvygcm68m0kwcsmbm2a6463azh5b89f200bylbqg3";
    };
  }
  {
    goPackagePath = "github.com/mdellandrea/golang-pkg-pcre";
    fetch = {
      type = "git";
      url = "https://github.com/mdellandrea/golang-pkg-pcre";
      rev = "72b7ffe8d2be809f820e18c73c0df84042895ff0";
      sha256 = "17r9dgfiifxl9ifj1svvrdsbdcdnqhx1qrkcmm934a9jkxyqc7li";
    };
  }
]

