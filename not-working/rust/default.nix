
let

  pkgs = import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/19.03.tar.gz") {};

in

pkgs.rustPlatform.buildRustPackage rec {
  name = "my-own-benchmarks-game-rust";
  src = ./.;
  buildInputs = [ pkgs.gmp ];
  cargoSha256 = "1mv7kdrfs477q5ii20pwdk8c395d68sy2a0bcnay9df07d3n9032";
}

