
let

  allScripts = [
    "python-mandelbrot-07"
  ];

  pkgs = import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/19.03.tar.gz") {};

  python = pkgs.python37.withPackages (ps: with ps; [ gmpy2 ]);

  addScript = aux: script: aux + ''
    printf "#!${python.interpreter}\n\n" > $out/bin/${script}
    chmod +x $out/bin/${script}
    cat src/${script}.py >> $out/bin/${script}
  '';

  foldl = pkgs.lib.lists.foldl;

in

  pkgs.stdenv.mkDerivation {
    name = "my-own-benchmarks-game-python";
    src = ./.;

    installPhase = ''
      mkdir -p $out/bin
    '' + foldl addScript "" allScripts;

  }

