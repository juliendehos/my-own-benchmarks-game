
let

  pkgs = import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/19.03.tar.gz") {};

  ghc = pkgs.haskell.packages.ghc864.ghcWithPackages (ps: [
    ps.async
    ps.hashtables
    ps.parallel
    ps.regex-pcre
  ]);

  opts = "--make -fllvm -O2 -threaded -rtsopts";

in

  pkgs.stdenv.mkDerivation {
    name = "my-own-benchmarks-game-haskell";
    src = ./.;

    buildInputs = [
      ghc
      pkgs.llvm_6
    ];

    buildPhase = ''


      ghc -o haskell-binarytrees-04 ${opts} -with-rtsopts="-N4 -K128M -H" src/haskell-binarytrees-04.hs

      ghc -o haskell-fannkuchredux-03 ${opts} -with-rtsopts="-N4" src/haskell-fannkuchredux-03.hs

      ghc -o haskell-mandelbrot-02 ${opts} -with-rtsopts="-N4" src/haskell-mandelbrot-02.hs

      ghc -o haskell-regexredux-02 ${opts} -with-rtsopts="-N4 -H250M" src/haskell-regexredux-02.hs

    '';

    installPhase = ''
      mkdir -p $out/bin
      cp haskell-* $out/bin
    '';

    shellHook = "eval $(egrep ^export ${ghc}/bin/ghc)";
  }

