
let

  pkgs = import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/19.03.tar.gz") {};

  stdenv = pkgs.overrideCC pkgs.stdenv pkgs.gcc8;

  opts = "-pipe -O3 -fomit-frame-pointer -march=native";

in

  stdenv.mkDerivation {
    name = "my-own-benchmarks-game-cpp";
    src = ./.;

    buildInputs = [
      pkgs.apr
      pkgs.boost
      pkgs.gmpxx
      pkgs.pcre
      pkgs.pkgconfig
      pkgs.re2
    ];

    buildPhase = ''

      g++ -o cpp-pidigits-03 ${opts} src/cpp-pidigits-03.cpp -lgmp -lgmpxx 

    '';

    installPhase = ''
      mkdir -p $out/bin
      cp cpp-* $out/bin
    '';
  }

