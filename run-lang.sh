#!/bin/sh

if [ $# -lt 2 ] ; then
    echo "usage: $0 <nb runs> <lang>"
    echo "example: $0 2 rust"
    exit
fi

NRUNS=$1
lang=$2

rm -rf build/code/$lang
./scripts/bench.sh $NRUNS output/output-$lang `find code/$lang -name "$lang-*-*.*"`

./scripts/merge.sh output/all output/output-*
./scripts/plot.py output/all
./scripts/render.hs output/all

