let
  pkgs = import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/19.03.tar.gz") {};
  stdenv = pkgs.overrideCC pkgs.stdenv pkgs.gcc8;
in

  stdenv.mkDerivation {
    name = "fasta";
    src = ./.;
    buildPhase = "gcc -o fasta -std=c11 -O2 -fopenmp fasta.c";
    installPhase = ''
      mkdir -p $out/bin
      cp fasta $out/bin/
    '';
  }

