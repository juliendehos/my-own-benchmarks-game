#!/bin/sh

if [ $# -lt 2 ] ; then
    echo "usage: $0 <merged dir> <dirs to merge...>"
    echo "example: $0 output output-*"
    exit
fi

OUTDIR=$1
mkdir -p $1
shift
OUTCSV="$OUTDIR/results.csv"

if ! test -f "$OUTCSV" ; then
    echo "creating $OUTCSV..."
    echo "lang,algo,rev,iter,time,memory,cpu" > $OUTCSV
else
    echo "appending to $OUTCSV..."
fi

for d in "$@" ; do
    RESULTSCSV="$d/results.csv"
    if ! test -f "$RESULTSCSV" ; then
        echo "$RESULTSCSV not found"
    else
        echo "$RESULTSCSV ok"
        tail -n +2 "$RESULTSCSV" >> "$OUTCSV"
    fi
done

