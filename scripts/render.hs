#!/usr/bin/env nix-shell
#!nix-shell -i runghc -p "haskellPackages.ghcWithPackages (pkgs: with pkgs; [ lucid split text turtle ])"

{-# LANGUAGE OverloadedStrings #-}
import Control.Monad (forM_)
import qualified Control.Foldl as Fold
import Data.List.Split (splitOn)
import Data.List (nub, sort)
import qualified Data.Text as T
import Data.Time (getCurrentTime)
import Lucid
import System.Environment (getArgs, getProgName)
import Turtle hiding (nub, sort)

resultscsv = "results.csv"

myPage algos time = do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            title_ "My-own-benchmarks-game"
            style_ "html { font-family: sans-serif; }" -- TODO use clay ?
        body_ $ do
            h1_ "My-own-benchmarks-game"
            p_ $ toHtml $ show time
            h2_ "Reference"
            ul_ $ do
                li_ $ a_ [href_ "https://gitlab.com/juliendehos/my-own-benchmarks-game"] "repository"
                li_ $ a_ [href_ resultscsv] $ toHtml resultscsv
                li_ $ a_ [href_ "https://benchmarksgame-team.pages.debian.net/benchmarksgame/"] "the computer benchmarks game"
            h2_ "Summary"
            myImg "Time (in s):" "files/summary-time.svg"
            myImg "Memory (in KB):" "files/summary-memory.svg"
            myImg "CPU (in %):" "files/summary-cpu.svg"
            h2_ "Algorithms"
            ul_ $ forM_ algos $ \ a -> li_ $ a_ [href_ $ "#" <> a] $ toHtml a
            forM_ algos $ \ a -> do
                h3_ [id_ a] $ toHtml a
                myImg "Time:" $ "files/algorithm-" <> a <> "-time.svg"
                myImg "Memory:" $ "files/algorithm-" <> a <> "-memory.svg"
                myImg "CPU:" $ "files/algorithm-" <> a <> "-cpu.svg"
    where myImg label src = p_ label >> img_ [src_ src]

main = do
    args <- getArgs
    if length args /= 1
    then do
        progName <- getProgName
        putStrLn $ "usage: ./" <> progName <> " <working directory>"
    else do
        time <- getCurrentTime
        cd $ decodeString $ head args
        let prefixPatt = "./files/algorithm-"
        images <- fold (find (begins prefixPatt >> ends ".svg") ".") Fold.list
        let algos = sort $ nub $ map (T.pack . (!!1) . splitOn "-" . encodeString) images
        print algos
        renderToFile "index.html" $ myPage algos time
        view $ realpath "index.html"

