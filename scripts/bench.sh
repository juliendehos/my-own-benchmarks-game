#!/usr/bin/env bash

###############################################################################
# command line arguments and main parameters
###############################################################################

if [ $# -lt 3 ] ; then
    echo "usage: $0 <nb runs> <output dir> <code files...>"
    echo "example: $0 2 output-rust \`find code -name \"rust-*-*.*\"\`"
    exit
fi

NRUNS=$1

shift
INITIALDIR=`pwd`
mkdir -p $1
OUTCSV="$INITIALDIR/$1/results.csv"

shift
CODEFILES=$@

SCRIPTDIR=`dirname $0 | xargs realpath`
ROOTDIR=`realpath $SCRIPTDIR/..`
BUILDDIR="$INITIALDIR/build"
mkdir -p "$BUILDDIR"
mkdir -p "$BUILDDIR/fasta"
mkdir -p "$BUILDDIR/code"

###############################################################################
# build fasta and generate input data for knucleotide, regexredux and revcomp
###############################################################################

FASTAPARAMS="1000 5000000 25000000"
cd "$BUILDDIR/fasta"

# build the fasta binary, if it does not exist yet
if ! test -f "./result/bin/fasta" ; then
    printf "building build/fasta... "
    nix-build $SCRIPTDIR/fasta/default.nix &> /dev/null && echo "ok" || echo "failed"
else
    echo "skipping build/fasta"
fi

# run fasta in order to generate data files, if they do not exist yet 
for p in $FASTAPARAMS ; do
    FASTAOUTFILE="fasta$p.txt"
    if ! test -f $FASTAOUTFILE ; then
        printf "building build/fasta/$FASTAOUTFILE... "
        ./result/bin/fasta $p > $FASTAOUTFILE && echo "ok" || echo "failed"
    else
        echo "skipping build/fasta/$FASTAOUTFILE"
    fi
done

###############################################################################
# build programs
###############################################################################

for CODEFILE in $CODEFILES ; do
    lang=`basename $CODEFILE | cut -d "." -f 1 | cut -d "-" -f 1`
    if ! test -d "$BUILDDIR/code/$lang" ; then
        mkdir -p "$BUILDDIR/code/$lang"
        cd "$BUILDDIR/code/$lang"
        printf "building build/code/$lang... "
        nix-build "$ROOTDIR/code/$lang/default.nix" &> /dev/null && echo "ok" || echo "error (build failed)"
    fi
done

###############################################################################
# bench programs
###############################################################################

TMPCSV=`mktemp`

for CODEFILE in $CODEFILES ; do

    # find lang/algo/rev from code file
    CODEBASE=`basename $CODEFILE | cut -d "." -f 1` 
    lang=`echo $CODEBASE | cut -d "-" -f 1`
    ALGO=`echo $CODEBASE | cut -d "-" -f 2`
    REV=`echo $CODEBASE | cut -d "-" -f 3`
    CODE="$lang-$ALGO-$REV"

    # find args and stdin for running the program to bench
    TEST_ARGS=""
    PERF_ARGS=""
    if test -f "$ROOTDIR/data/$ALGO-test.args" ; then
        TEST_ARGS=`cat $ROOTDIR/data/$ALGO-test.args`
        PERF_ARGS=`cat $ROOTDIR/data/$ALGO-perf.args`
    fi
    TEST_STDIN="/dev/zero"
    PERF_STDIN="/dev/zero"
    if test -f "$ROOTDIR/data/$ALGO-test.stdin" ; then
        TEST_TMP=`cat $ROOTDIR/data/$ALGO-test.stdin`
        TEST_STDIN="$BUILDDIR/fasta/$TEST_TMP"
        PERF_TMP=`cat $ROOTDIR/data/$ALGO-perf.stdin`
        PERF_STDIN="$BUILDDIR/fasta/$PERF_TMP"
    fi
    TEST_OUT="$BUILDDIR/code/$lang/$CODE.out"

    # bench program
    cd "$BUILDDIR/code/$lang"
    printf "test $CODE $TEST_ARGS < `basename $TEST_STDIN`... "
    if ! ./result/bin/$CODE $TEST_ARGS < $TEST_STDIN > $TEST_OUT ; then
        >&2 echo "error (test failed)"
    else
        if ! $ROOTDIR/data/$ALGO-diff.sh $ROOTDIR/data/$ALGO-test.out $TEST_OUT &> /dev/null ; then
            >&2 echo "error (bad output)"
        else
            echo "ok"
            for n in `seq $NRUNS` ; do
                printf "perf $CODE $PERF_ARGS < `basename $PERF_STDIN` ($n/$NRUNS)... "
                env time -a -o $TMPCSV -f "$lang,$ALGO,$REV,$n,%e,%M,%P" ./result/bin/$CODE $PERF_ARGS < $PERF_STDIN &> /dev/null && echo "ok" || echo "error (perf failed)"
            done
        fi
    fi
done

###############################################################################
# generate final CSV file
###############################################################################

echo "lang,algo,rev,iter,time,memory,cpu" > $OUTCSV
sed 's/%//' < $TMPCSV >> $OUTCSV
rm $TMPCSV

