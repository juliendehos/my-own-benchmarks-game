#! /usr/bin/env nix-shell
#! nix-shell -i python3 -p "python3.withPackages(ps: [ps.matplotlib ps.pandas])"

from matplotlib import pyplot as plt
import os
import numpy as np
import pandas as pd
import shutil
import sys

RESULTSCSV = "results.csv"
BESTSCSV = "bests.csv"

def plotSummary(df, col, outdir):
    plt.close()
    pv = df.pivot("algo", "lang", col)
    ax = pv.plot(kind="barh", figsize=(12, 10))
    ax.set_ylabel(None)
    plt.xscale("log")
    plt.gca().invert_yaxis()
    ax.set_xlabel(None)
    ax.grid(axis="x", which="minor", alpha=0.4)
    ax.grid(axis="x", which="major", alpha=1.0)
    plt.legend(loc="upper right")
    filename = outdir+"/summary-"+col+".svg"
    print(filename)
    plt.savefig(filename, bbox_inches="tight")

def plotAlgorithm(df0, algo, col, mycolor, outdir):
    plt.close()
    df = df0.loc[df0["algo"] == algo]
    fig = plt.figure(figsize=(12, 8))
    ax = fig.add_subplot(111)
    ax.barh(df["lang-rev"], df[col], color=mycolor)
    #plt.xscale("log")
    #plt.xlim(left=0.1)
    ax.grid(axis="x", which="minor", alpha=0.4)
    ax.grid(axis="x", which="major", alpha=1.0)
    filename = outdir+"/algorithm-{}-{}.svg".format(algo, col)
    print(filename)
    plt.savefig(filename, bbox_inches="tight")

def main(workdir):
    # prepare output directory
    outdir = workdir + "/files"
    shutil.rmtree(outdir, ignore_errors=True)
    os.mkdir(outdir)
    # read data
    df = pd.read_csv("{}/{}".format(workdir, RESULTSCSV), header=0, sep=",")
    algos = df["algo"].drop_duplicates()
    # plot summary
    dfCodes = df.sort_values("time", ascending=True).drop_duplicates(["lang", "algo"])
    dfCodes.to_csv("{}/{}".format(workdir, BESTSCSV))
    plotSummary(dfCodes, "time", outdir)
    plotSummary(dfCodes, "memory", outdir)
    plotSummary(dfCodes, "cpu", outdir)
    # plot algorithms
    dfRuns = df.sort_values("time", ascending=True).drop_duplicates(["lang", "algo", "rev"])
    dfRuns["lang-rev"] = dfRuns["lang"] + dfRuns["rev"].map(lambda x: "-{:02d}".format(x))
    dfRuns = dfRuns.sort_values("lang-rev", ascending=True)
    for algo in algos:
        plotAlgorithm(dfRuns, algo, "time", "red", outdir)
        plotAlgorithm(dfRuns, algo, "memory", "green", outdir)
        plotAlgorithm(dfRuns, algo, "cpu", "blue", outdir)

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("usage: {} <working directory>".format(sys.argv[0]))
    else:
        main(sys.argv[1])

