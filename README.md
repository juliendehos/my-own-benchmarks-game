# my-own-benchmarks-game

**WORK IN PROGRESS**

This project aims to implement
[the computer benchmarks game](https://salsa.debian.org/benchmarksgame-team/benchmarksgame)
in a more reproducible and collaborative way. See some results 
[here](https://juliendehos.gitlab.io/my-own-benchmarks-game/).

Major changes from the 
[original project](https://benchmarksgame-team.pages.debian.net/benchmarksgame/):

- the source code is handled by git, directly (not packed in a zip file)
- the programs are built, run and benchmarked using only a few simple bash
  scripts
- the programs are configured using [Nix](https://nixos.org/nix/), so the
  project should be easy to use, i.e. git clone + install nix + run one bash
  script 

## Usage

- install [Nix](https://nixos.org/nix/) or [NixOS](https://nixos.org/)
- install GNU time and numdiff: `nix-env -iA nixos.time nixos.numdiff`
- run benchmarks: `./run.sh`
- see results: `firefox output/all/index.html

## Add a code file

- add the code file in a folder `code/lang/...`
- write/update the `default.nix` to build an executable `bin/lang-algo-rev`
- benchmark the program using `./scripts/bench.sh 2 output $(find code -name "lang-algo-rev.*")`

## TODO

- limit number of workers (4)
- metrics (currently: best run/rev)
- plot file size/loc
- nim ? https://github.com/def-/nim-benchmarksgame
- crystal ? https://github.com/kostya/crystal-benchmarks-game
- ada chapel csharp dart erlang fortran fsharp haskell java julia lisp
  lua ocaml pascal perl pharo php python racket ruby smalltalk swift

