#!/bin/sh

files=`find . -name "*.$1"`

for f in $files ; do
    b1=`echo $f | cut -d "/" -f 2`
    b2=`echo $b1 | cut -d "." -f 1`
    n1=`echo $b1 | cut -d "." -f 2`
    n2=`echo $n1 | cut -d "-" -f 2`
    if [ "$n2" == "$1" ] ; then
        mv $f $b2-01.$2
    else
        mv $f $b2-0$n2.$2
    fi
done

