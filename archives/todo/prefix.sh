#!/bin/sh

if [ $# -lt 1 ] ; then
    echo "usage: $0 <prefix> <code files...>"
    echo "example: $0 c c/*.c"
    exit
fi

prefix=$1
shift
files=$@

for file in $files ; do
    d=`dirname $file`
    f=`basename $file`
    echo "$file -> $d/$prefix-$f"
    git mv "$file" "$d/$prefix-$f"
done

