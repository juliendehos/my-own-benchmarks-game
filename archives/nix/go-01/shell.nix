with import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/19.03.tar.gz") {};

buildGoPackage {
  name = "my-own-benchmarks-game-go-01";
  src = ./. ;
  goPackagePath = "gitlab.com/juliendehos/my-own-benchmarks-game";
  goDeps = ./deps.nix;
  buildInputs = [
    gmp
    pcre
  ];
}

