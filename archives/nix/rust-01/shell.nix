with import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/19.03.tar.gz") {};

stdenv.mkDerivation {

  name = "my-own-benchmarks-games-rust-01";

  nativeBuildInputs = [
    rustc
  ];

  buildInputs = [
    gmp
  ];
}

