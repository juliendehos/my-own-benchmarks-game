{ nixpkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/19.03.tar.gz"){}
, compiler ? "ghc864"
}:

let

  inherit (nixpkgs) pkgs;

  ghc = pkgs.haskell.packages.${compiler}.ghcWithPackages (ps: with ps; [
    async
    parallel
    regex-pcre
    hashtables
  ]);

in

  pkgs.stdenv.mkDerivation {
    name = "my-own-benchmarks-game-haskell-01";
    buildInputs = [ ghc ];
    shellHook = "eval $(egrep ^export ${ghc}/bin/ghc)";
  }

