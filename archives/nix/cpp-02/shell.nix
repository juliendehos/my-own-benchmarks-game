with import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/19.03.tar.gz") {};

let

  _clang = clang_7;

  _stdenv = overrideCC stdenv _clang;

in

_stdenv.mkDerivation {
  name = "my-own-benchmarks-games-cpp-02";
  src = ./.;
  buildInputs = [
    boost
    gmpxx
    _clang
    llvmPackages.openmp
    pkgconfig
  ];
}


