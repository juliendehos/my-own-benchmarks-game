with import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/19.03.tar.gz") {};

mkShell {
  buildInputs = [
    julia_11
  ];
  shellHook = ''
    julia -e 'import Pkg; Pkg.add("Match")'
  '';
}

