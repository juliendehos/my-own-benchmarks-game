with import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/19.03.tar.gz") {};

mkShell {
  buildInputs = [
    python3.pkgs.gmpy2
  ];
}

