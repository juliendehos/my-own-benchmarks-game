with import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/19.03.tar.gz") {};

let

  _stdenv = overrideCC stdenv gcc8;

in

_stdenv.mkDerivation {
  name = "my-own-benchmarks-games-cpp-01";
  src = ./.;
  buildInputs = [
    boost
    gcc
    gmpxx
    pkgconfig
  ];
}


